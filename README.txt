

Build instructions

    Run the following in the base directory of the project. 
        ./gradlew build 
    This will run the tests and build the project.

    The war file can be found under build/libs.
    
Deploy

    Install Java 8
        sudo apt-get install default-jdk
        
    Install TomCat
    
        wget http://apache.cs.utah.edu/tomcat/tomcat-8/v8.5.34/bin/apache-tomcat-8.5.34.zip
        unzip apache-tomcat-8.5.34.zip
        sudo mv apache-tomcat-8.5.34 /opt/tomcat
        cd /opt/tomcat/bin
        chmod 744 *sh
        
    Install ParkPay
    
        Rename the war file to what ever you wish your {path} to be.
        
        Then place the war file into /opt/tomcat/webapps
        
        
    Launching TomCat
    
        /opt/tomcat/bin/startup.sh
        
        
TEST REPORTS

    The test reports are available under build\reports\jacoco\test\html\ after building.
    
    
This project is free to use under the MIT License

Copyright (c) 2018 William Gaylord

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

