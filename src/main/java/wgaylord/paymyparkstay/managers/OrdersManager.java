package wgaylord.paymyparkstay.managers;

import wgaylord.paymyparkstay.interfaces.OrdersBoundaryInterface;
import wgaylord.paymyparkstay.objects.Order;

import java.util.UUID;
import java.util.Vector;

public class OrdersManager implements OrdersBoundaryInterface {

    static Vector<Order> orders = new Vector<>();

    public OrdersManager(){

    }

    @Override
    public Order[] getOrders() {
        return orders.toArray(new Order[0]);
    }

    @Override
    public void addOrder(Order order) {
        orders.add(order);
    }

    @Override
    public Order[] searchOrders(String keyword) {
        Vector<Order> output = new Vector<>();

        for (Order visiter : orders) {
            if (visiter.getPlate().contains(keyword)) {
                output.add(visiter);
            }
        }
        return output.toArray(new Order[0]);
    }

    @Override
    public Order getOrder(UUID id) {
        for (Order order : orders) {
            if (order.getId().equals(id)) {
                return order;
            }
        }
        return null;
    }

    @Override
    public Order[] getOrdersFromVistor(UUID id) {
        Vector<Order> output = new Vector<>();

        for (Order order : orders) {
            if (order.getVid().equals(id)) {
                output.add(order);
            }
        }
        return output.toArray(new Order[0]);
    }

    @Override
    public Order[] getOrdersFromVistorAndPark(UUID vid, UUID pid) {
        Vector<Order> output = new Vector<>();

        for (Order order : orders) {
            if (order.getVid().equals(vid) & order.getPid().equals(pid)) {
                output.add(order);
            }
        }
        return output.toArray(new Order[0]);
    }

    @Override
    public Order[] getOrdersFromPark(UUID id) {
        Vector<Order> output = new Vector<>();

        for (Order order : orders) {
            if (order.getPid().equals(id)) {
                output.add(order);
            }
        }
        return output.toArray(new Order[0]);
    }

}
