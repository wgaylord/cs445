package wgaylord.paymyparkstay.managers;

import wgaylord.paymyparkstay.interfaces.VisitorBoundaryInterface;
import wgaylord.paymyparkstay.objects.Visitor;

import java.util.UUID;
import java.util.Vector;

public class VisitorsManager implements VisitorBoundaryInterface {

    public static Vector<Visitor> visitors = new Vector<>() ;

    public VisitorsManager(){

    }

    public Visitor[] getVisitors() {
        return visitors.toArray(new Visitor[0]);
    }

    public void addVisitor(Visitor visitor) {

        if(this.getVisitor(visitor.getId()) == null){
            visitors.add(visitor);
         }else{
            if(visitor.getName().length() > 0) {
                Visitor vist = this.getVisitor(visitor.getId());
                visitors.remove(vist);
                vist.setName(visitor.getName());
                visitors.add(vist);
            }
        }



    }

    public Visitor[] searchVisitors(String keyword) {
        Vector<Visitor> output = new Vector<>();

        for (Visitor visiter : visitors) {
            if (visiter.getEmail().contains(keyword) || visiter.getName().contains(keyword)) {
                output.add(visiter);
            }
        }
        return output.toArray(new Visitor[0]);
    }

    public Visitor getVisitor(UUID id) {
        for (Visitor visiter : visitors) {
            if (visiter.getId().equals(id)) {
                return visiter;
            }
        }
        return null;

    }
}
