package wgaylord.paymyparkstay.managers;

import wgaylord.paymyparkstay.interfaces.NotesBoundaryInterface;
import wgaylord.paymyparkstay.objects.Note;

import java.util.UUID;
import java.util.Vector;

public class NotesManager implements NotesBoundaryInterface{

    public static Vector<Note> notes = new Vector<>();

    public NotesManager(){
    }

    @Override
    public Note[] getNotes() {
        Vector<Note> noteArray = new Vector<>();

        for (Note note : notes) {
            if (!note.isDeleted()) {
                noteArray.add(note);
            }
        }
        return noteArray.toArray(new Note[0]);
    }

    @Override
    public void addNote(Note note) {
        if(this.getNote(note.getId()) == null){
            notes.add(note);
        }else{
            notes.remove(this.getNote(note.getId()));
            notes.add(note);
        }

    }

    @Override
    public Note[] searchNotes(String keyword) {
        Vector<Note> output = new Vector<>();

        for (Note note : notes) {
            if (note.getTitle().contains(keyword) || note.getMessage().contains(keyword)) {
                output.add(note);
            }
        }
        return output.toArray(new Note[0]);
    }

    @Override
    public Note getNote(UUID id) {
        for (Note note : notes) {
            if (note.getId().equals(id)) {
                return note;
            }
        }
        return null;
    }
    @Override
    public Note[] getNotesFromPark(UUID id) {
        Vector<Note> output = new Vector<>();

        for (Note note : notes) {
            if (note.getPid().equals(id)) {
                output.add(note);
            }
        }
        return output.toArray(new Note[0]);
    }

    @Override
    public Note[] getNotesFromVistor(UUID id) {
        Vector<Note> output = new Vector<>();

        for (Note note : notes) {
            if (note.getVid().equals(id)) {
                output.add(note);
            }
        }
        return output.toArray(new Note[0]);
    }
}
