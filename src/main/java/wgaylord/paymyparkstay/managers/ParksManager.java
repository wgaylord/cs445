package wgaylord.paymyparkstay.managers;

import wgaylord.paymyparkstay.interfaces.ParksBoundaryInterface;
import wgaylord.paymyparkstay.objects.Park;

import java.util.UUID;
import java.util.Vector;

public class ParksManager implements ParksBoundaryInterface {

    public static  Vector<Park> parks = new Vector<>();

    public ParksManager() {



    }

    @Override
    public Park[] getParks() {
        Vector<Park> parkArray = new Vector<>();

        for (Park park : parks) {
            if (park.isActive()) {
                parkArray.add(park);
            }
        }
        return parkArray.toArray(new Park[0]);
    }

    @Override
    public void addPark(Park park) {

        if(this.getPark(park.getID()) != null){
            parks.remove(this.getPark(park.getID()));

        }
        parks.add(park);

    }

    @Override
    public Park[] searchParks(String keyword) {
        Vector<Park> output = new Vector<>();

        for (Park park : parks) {
            if (park.getAddress().toLowerCase().contains(keyword.toLowerCase()) || park.getName().toLowerCase().contains(keyword.toLowerCase()) || park.getRegion().toLowerCase().contains(keyword.toLowerCase())) {
                if (park.isActive()) {
                    output.add(park);
                }
            }
        }
        return output.toArray(new Park[0]);
    }

    @Override
    public Park getPark(UUID id) {
        System.out.println(parks.size());
        for (Park park : parks) {
            if (park.getID().equals(id)) {
                return park;
            }
        }
        return null;
    }

    @Override
    public void deletePark(UUID id) {
        Park temp = this.getPark(id);
        temp.setActive(false);
        this.addPark(temp);
    }



}