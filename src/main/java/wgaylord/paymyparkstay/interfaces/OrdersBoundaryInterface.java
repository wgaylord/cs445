package wgaylord.paymyparkstay.interfaces;

import wgaylord.paymyparkstay.objects.Order;

import java.util.UUID;

public interface OrdersBoundaryInterface {

    Order[] getOrders();
    void addOrder(Order order);
    Order[] searchOrders(String keyword);
    Order getOrder(UUID id);
    Order[] getOrdersFromVistor(UUID id);
    Order[] getOrdersFromVistorAndPark(UUID vid,UUID pid);

    Order[] getOrdersFromPark(UUID id);
}
