package wgaylord.paymyparkstay.interfaces;

import wgaylord.paymyparkstay.objects.Visitor;

import java.util.UUID;

public interface VisitorBoundaryInterface {

    Visitor[] getVisitors();
    void addVisitor(Visitor visitor);
    Visitor[] searchVisitors(String keyword);
    Visitor getVisitor(UUID id);

}
