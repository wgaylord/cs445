package wgaylord.paymyparkstay.interfaces;

import wgaylord.paymyparkstay.objects.Park;

import java.util.UUID;

@SuppressWarnings("unused")
public interface ParksBoundaryInterface {
    Park[] getParks();
    void addPark(Park visitor);
    Park[] searchParks(String keyword);
    Park getPark(UUID id);
    void deletePark(UUID id);


}
