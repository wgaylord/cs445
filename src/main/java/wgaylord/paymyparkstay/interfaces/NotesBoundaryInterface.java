package wgaylord.paymyparkstay.interfaces;

import wgaylord.paymyparkstay.objects.Note;

import java.util.UUID;

public interface NotesBoundaryInterface {
    Note[] getNotes();
    void addNote(Note note);
    Note[] searchNotes(String keyword);
    Note getNote(UUID id);
    Note[] getNotesFromVistor(UUID id);
    Note[] getNotesFromPark(UUID id);

}
