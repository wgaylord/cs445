package wgaylord.paymyparkstay.api;

import wgaylord.paymyparkstay.interfaces.OrdersBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.ParksBoundaryInterface;
import wgaylord.paymyparkstay.managers.OrdersManager;
import wgaylord.paymyparkstay.managers.ParksManager;
import wgaylord.paymyparkstay.objects.Order;
import wgaylord.paymyparkstay.objects.Park;

import javax.json.*;

class ReportGenerator {
    private static final OrdersBoundaryInterface obi = new OrdersManager();
    private static final ParksBoundaryInterface pbi = new ParksManager();

    public static JsonArray getReports(){
        JsonArrayBuilder reports = Json.createArrayBuilder();

        JsonObjectBuilder admsions = Json.createObjectBuilder();
        admsions.add("rid",907);
        admsions.add("name","Admissions report");

        JsonObjectBuilder revenue = Json.createObjectBuilder();
        revenue.add("rid",911);
        revenue.add("name","Revenue report");

        reports.add(admsions);
        reports.add(revenue);

        return reports.build();
    }

    public static JsonObject getReport(int id,String start,String stop) {
        switch(id){
            case 907:
               return getAdmissionsReport(start,stop);
            case 911:
                return getRevenueReport(start,stop);
        }
        return null;
    }

    private static JsonObject getAdmissionsReport(String start, String stop){
        int total_admission = 0;
        JsonObjectBuilder Jreport = Json.createObjectBuilder();
        Jreport.add("rid",907);
        Jreport.add("name","Admissions report");
        Jreport.add("start_date",start);
        Jreport.add("end_date",stop);

        JsonArrayBuilder parkList = Json.createArrayBuilder();

        Park[] parks = pbi.getParks();
        for (Park park1 : parks) {
            JsonObjectBuilder park = Json.createObjectBuilder();
            int park_admission = 0;
            Order[] orders = obi.getOrdersFromPark(park1.getID());
            for (Order order : orders) {
                park_admission++;
            }
            park.add("pid", park1.getID().toString());
            park.add("name", park1.getName());
            park.add("admissions", park_admission);
            total_admission += park_admission;
            parkList.add(park.build());
        }
        Jreport.add("total_admissions",total_admission);
        Jreport.add("detail_by_park",parkList.build());
        return Jreport.build();


    }

    private static JsonObject getRevenueReport(String start, String stop){
        double total_revenue = 0;
        int total_orders = 0;
        JsonObjectBuilder Jreport = Json.createObjectBuilder();
        Jreport.add("rid",911);
        Jreport.add("name","Revenue report");
        Jreport.add("start_date",start);
        Jreport.add("end_date",stop);

        JsonArrayBuilder parkList = Json.createArrayBuilder();

        Park[] parks = pbi.getParks();
        for (Park park1 : parks) {
            JsonObjectBuilder park = Json.createObjectBuilder();
            double park_revenue = 0;
            Order[] orders = obi.getOrdersFromPark(park1.getID());
            for (Order order : orders) {
                park_revenue += order.getAmount();
                total_orders++;
            }
            park.add("pid", park1.getID().toString());
            park.add("name", park1.getName());
            park.add("orders",orders.length);
            park.add("revenue", park_revenue);
            total_revenue += park_revenue;
            parkList.add(park.build());
        }
        Jreport.add("total_revenue",total_revenue);
        Jreport.add("detail_by_park",parkList.build());
        Jreport.add("total_orders",total_orders);

        return Jreport.build();


    }


}
