package wgaylord.paymyparkstay.api;


import wgaylord.paymyparkstay.interfaces.NotesBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.OrdersBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.ParksBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.VisitorBoundaryInterface;
import wgaylord.paymyparkstay.managers.NotesManager;
import wgaylord.paymyparkstay.managers.OrdersManager;
import wgaylord.paymyparkstay.managers.ParksManager;
import wgaylord.paymyparkstay.managers.VisitorsManager;

import javax.json.*;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import wgaylord.paymyparkstay.objects.Note;
import wgaylord.paymyparkstay.objects.Order;
import wgaylord.paymyparkstay.objects.Park;
import wgaylord.paymyparkstay.objects.Visitor;

import java.io.StringReader;
import java.util.Date;
import java.util.UUID;

@Path("")
public class Server  {

    private final NotesBoundaryInterface nbi = new NotesManager();
    private final OrdersBoundaryInterface obi = new OrdersManager();
    private final ParksBoundaryInterface pbi = new ParksManager();
    private final VisitorBoundaryInterface vbi = new VisitorsManager();


    @GET @Path("/parks")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllParks(@Context UriInfo info) {
        Park[] parks = pbi.getParks();
        String key = info.getQueryParameters().getFirst("key");
        if(key != null){
            parks = pbi.searchParks(key);
        }
        JsonArrayBuilder Parks = Json.createArrayBuilder();
        for (Park park : parks) {
            Parks.add(JsonSerializer.SerializeParkView(park));
        }
        return Response.status(Response.Status.OK).entity(Parks.build().toString()).build();
    }


    @POST @Path("/parks")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPark(@Context UriInfo uriInfo, String json) {
        JsonObject jobj;
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(json));
            jobj = jsonReader.readObject();
        }catch(JsonParsingException exeption){
            return JsonSerializer.buildDataValidationError("Error prasing posted json","/parks");
        }

        Park newPark = new Park();
        try {
            newPark = JsonDeserializer.DeserializerPark(jobj,newPark);
        }catch(RuntimeException exception){
            return JsonSerializer.buildDataValidationError(exception.getMessage(),"/parks");
        }
        pbi.addPark(newPark);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path("parks/"+newPark.getID().toString());

        JsonObjectBuilder reponse = Json.createObjectBuilder();
        reponse.add("pid",newPark.getID().toString());

        return Response.created(builder.build()).entity(reponse.build().toString()).build();

    }


    @PUT @Path("/parks/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response UpdatePark(@Context UriInfo uriInfo, String json,@PathParam("id") String parkID) {
        JsonObject jobj;
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(json));
            jobj = jsonReader.readObject();

        }catch(JsonParsingException exeption){
            return JsonSerializer.buildDataValidationError("Error prasing posted json","/parks");
        }

        Park park = pbi.getPark(UUID.fromString(parkID));

        if(park == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        try {
            park = JsonDeserializer.DeserializerPark(jobj,park);
        }catch(RuntimeException exception){
            return JsonSerializer.buildDataValidationError(exception.getMessage(),"/parks");
        }
        pbi.addPark(park);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path("parks/"+park.getID().toString());

        JsonObjectBuilder reponse = Json.createObjectBuilder();
        reponse.add("pid",park.getID().toString());

        return Response.created(builder.build()).entity(reponse.build().toString()).build();

    }


    @DELETE @Path("/parks/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePark(@PathParam("id") String parkID) {
        Park park = pbi.getPark(UUID.fromString(parkID));
        if(park == null){
            return Response.status((Response.Status.NOT_FOUND)).build();
        }
        if(park.isActive()) {
            park.setActive(false);
            pbi.addPark(park);
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.status((Response.Status.NOT_FOUND)).build();
    }


    @GET @Path("/parks/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPark(@PathParam("id") String parkID) {
        Park park = pbi.getPark(UUID.fromString(parkID));
        if (park == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else{
            return Response.status(Response.Status.OK).entity(JsonSerializer.SerializeParkDetail(park).toString()).build();
        }
    }


    @GET @Path("/parks/{id}/notes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getParkNotes(@PathParam("id") String parkID){
        Park park = pbi.getPark(UUID.fromString(parkID));
        if (park != null) {
            Note[] notes = nbi.getNotesFromPark(park.getID());
            JsonArrayBuilder Notes = Json.createArrayBuilder();
            for (Note note : notes) {
                Notes.add(JsonSerializer.SerializeNoteForParks(note));
            }
            JsonArray jpark = Json.createArrayBuilder().add(Json.createObjectBuilder().add("pid",parkID).add("notes",Notes.build()).build()).build();
            return Response.status(Response.Status.OK).entity(jpark.toString()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();

    }


    @GET @Path("/parks/{id}/notes/{nid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getParkNote(@PathParam("nid") String noteID){
                Note note = nbi.getNote(UUID.fromString(noteID));
         if(note != null & !note.isDeleted()){
            return Response.status(Response.Status.OK).entity(JsonSerializer.SerializeParkNote(note).toString()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @POST @Path("/parks/{id}/notes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createParkNote(@Context UriInfo uriInfo,@PathParam("id") String parkID,String json){
        JsonObject jobj;
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(json));
            jobj = jsonReader.readObject();
        }catch(JsonParsingException exeption){
            return JsonSerializer.buildDataValidationError("Error prasing posted json","/parks/"+parkID);
        }
        String visitorID = jobj.getString("vid");
        String Text = jobj.getString("text");
        String Title = jobj.getString("title");

        Order[] orders = obi.getOrdersFromVistorAndPark(UUID.fromString(visitorID),UUID.fromString(parkID));
        if(orders.length > 0){
            Note note = new Note();
            note.setMessage(Text);
            note.setVid(UUID.fromString(visitorID));
            note.setPid(UUID.fromString(parkID));
            note.setTitle(Title);
            note.setDate(new Date());
            nbi.addNote(note);
            UriBuilder builder = uriInfo.getAbsolutePathBuilder();
            builder.path("parks/" + parkID + "/notes" + note.getId().toString());
            JsonObjectBuilder reponse = Json.createObjectBuilder();
            reponse.add("nid", note.getId().toString());
            return Response.created(builder.build()).entity(reponse.build().toString()).build();
        }
        return JsonSerializer.buildDataValidationError("You may not post a note to a park unless you paid for admission at that park","parks/"+parkID);
    }



    @GET @Path("/notes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllNotes(@Context UriInfo info){
        JsonArrayBuilder Notes =Json.createArrayBuilder();
        String key = info.getQueryParameters().getFirst("key");
        if(key == null) {
            Park[] parks = pbi.getParks();
            for (Park park : parks) {
                UUID parkID = park.getID();
                Note[] notes = nbi.getNotesFromPark(parkID);
                JsonArrayBuilder Pnotes = Json.createArrayBuilder();
                for (Note note1 : notes) {
                    JsonObjectBuilder note = Json.createObjectBuilder();
                    note.add("nid", note1.getId().toString());
                    note.add("date", note1.getDate().toString());
                    note.add("title", note1.getTitle());
                    Pnotes.add(note.build());
                }
                Notes.add(Json.createObjectBuilder().add("pid", parkID.toString()).add("notes", Pnotes));
            }
            return Response.status(Response.Status.OK).entity(Notes.build().toString()).build();
        }
        Park[] parks = pbi.getParks();
        for (Park park : parks) {
            UUID parkID = park.getID();
            Note[] notes = nbi.getNotesFromPark(parkID);
            JsonArrayBuilder Pnotes = Json.createArrayBuilder();
            for (Note note1 : notes) {
                if (note1.getTitle().contains(key) || note1.getMessage().contains(key)) {
                    JsonObjectBuilder note = Json.createObjectBuilder();
                    note.add("nid", note1.getId().toString());
                    note.add("date", note1.getDate().toString());
                    note.add("title", note1.getTitle());
                    Pnotes.add(note.build());
                }
            }
            Notes.add(Json.createObjectBuilder().add("pid", parkID.toString()).add("notes", Pnotes));
        }
        return Response.status(Response.Status.OK).entity(Notes.build().toString()).build();
    }


    @GET @Path("/notes/{nid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNote(@PathParam("nid") String noteID){
        Note note = nbi.getNote(UUID.fromString(noteID));
        if(note != null){
            return Response.status(Response.Status.OK).entity(JsonSerializer.SeralizeNoteDetails(note).toString()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @PUT @Path("/notes/{nid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateNote(@Context UriInfo uriInfo,@PathParam("nid") String noteID,String json){
        Note note = nbi.getNote(UUID.fromString(noteID));
        JsonObject jobj;
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(json));
            jobj = jsonReader.readObject();

        }catch(JsonParsingException exeption){
            return JsonSerializer.buildDataValidationError("Error prasing posted json","/notes/"+noteID);
        }
        String visitorID = jobj.getString("vid");
        String Text = jobj.getString("text");
        String Title = jobj.getString("title");
        if(visitorID != null & Text != null & Title != null){
        if(note != null){
            note.setTitle(Title);
            note.setMessage(Text);
            nbi.addNote(note);
            return Response.status(Response.Status.OK).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    } return JsonSerializer.buildDataValidationError("Error in note update","/notes/"+noteID);
    }



    @DELETE @Path("/notes/{nid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteNote(@PathParam("nid") String noteID){
        Note note = nbi.getNote(UUID.fromString(noteID));

            if(note != null){
               note.setDeleted();
               nbi.addNote(note);
                return Response.status(Response.Status.OK).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).build();
            }
    }

    @POST @Path("/orders")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOrder(@Context UriInfo info,String json){
        JsonObject jobj;
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(json));
            jobj = jsonReader.readObject();

        }catch(JsonParsingException exeption){
            return JsonSerializer.buildDataValidationError("Error prasing posted json","/orders");
        }
        String pid = jobj.getString("pid");
        JsonObject vechicle = jobj.getJsonObject("vehicle");
        JsonObject visitor = jobj.getJsonObject("visitor");
        if(pid != null & vechicle != null & visitor != null){
            Order order = new Order();
            order.setVist_date(new Date());
            order.setPid(UUID.fromString(pid));
            String state = vechicle.getString("state");
            String plate = vechicle.getString("plate");
            String type = vechicle.getString("type");
            String name = "";
            try {
                name = visitor.getString("name");
            }catch(NullPointerException excption){ //Name is optional nut Json throws null pointer if it does not exist
                 }
            String email = visitor.getString("email");
            JsonObject payment = visitor.getJsonObject("payment_info");

            if(state == null | plate == null | type ==null){
                return JsonSerializer.buildDataValidationError("Vehicle is missing information","/orders");
            }
            order.setPlate(plate);
            order.setState(state);
            order.setType(type);
            if(payment == null) {
                return JsonSerializer.buildDataValidationError("No payment info!", "/orders");
            }
            if(email == null){
                return JsonSerializer.buildDataValidationError("Missing Email","/orders");
            }
            Visitor vist = new Visitor(email);
            order.setVid(vist.getId());
            vist.setName(name);
            String card = payment.getString("card");
            String name_on_card = payment.getString("name_on_card");
            String expiration_date = payment.getString("expiration_date");
            int zip = payment.getInt("zip");
            double amount;
            if(pbi.getPark(UUID.fromString(pid)).getAddress().contains(state+" ")){
                amount = pbi.getPark(UUID.fromString(pid)).getFee(type)[0];
            }else{
                amount = pbi.getPark(UUID.fromString(pid)).getFee(type)[1];
            }
            order.setAmount(amount);
            order.setExpiration_date(expiration_date);
            order.setCard(card);
            order.setName_on_card(name_on_card);
            order.setZip(zip);
            order.setCard_transaction_id("123-4567-89");
            order.setDate_and_time(new Date());
            obi.addOrder(order);
            vbi.addVisitor(vist);
            UriBuilder builder = info.getAbsolutePathBuilder();
            builder.path("/orders/"+order.getId().toString());
            JsonObjectBuilder reponse = Json.createObjectBuilder();
            reponse.add("oid",order.getId().toString());
            return Response.created(builder.build()).entity(reponse.build().toString()).build();
        }else{
            return JsonSerializer.buildDataValidationError("One of the componets of the order are missing","/orders");
        }
    }


    @GET @Path("/orders")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrders(@Context UriInfo info){
        JsonArrayBuilder Orders =Json.createArrayBuilder();
        String key = info.getQueryParameters().getFirst("key");
            Order[] orders;
            if(key == null){
                orders = obi.getOrders();
            }else{
                orders = obi.searchOrders(key);
            }
        for (Order order : orders) {
            Orders.add(JsonSerializer.SerializeOrder(order));
        }
            return Response.status(Response.Status.OK).entity(Orders.build().toString()).build();
    }



    @GET @Path("/orders/{oid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrder(@PathParam("oid") String oid){
            Order order = obi.getOrder(UUID.fromString(oid))  ;
            if(order == null){
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        return Response.status(Response.Status.OK).entity(JsonSerializer.SerlizeOrderDetails(order).toString()).build();
    }


    @GET @Path("/visitors")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVisitorss(@Context UriInfo info) {
        JsonArrayBuilder Visitors = Json.createArrayBuilder();
        String key = info.getQueryParameters().getFirst("key");
        Visitor[] visitors;
        if (key == null) {
            visitors = vbi.getVisitors();
        } else {
            visitors = vbi.searchVisitors(key);
        }
        for (Visitor visitor : visitors) {
            Visitors.add(JsonSerializer.SerializeVisitor(visitor));
        }
        return Response.status(Response.Status.OK).entity(Visitors.build().toString()).build();
    }

    @GET @Path("/visitors/{vid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVisitor(@PathParam("vid") String vid){
        Visitor visitor = vbi.getVisitor(UUID.fromString(vid));
        if(visitor == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.OK).entity(JsonSerializer.SerlizeVisitorDetails(visitor).toString()).build();
    }
    @GET @Path("/reports")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReports(){
        return Response.status(Response.Status.OK).entity(ReportGenerator.getReports().toString()).build();
    }

    @GET @Path("/reports/{ID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReport(@Context UriInfo info,@PathParam("ID") int id){
        String start = info.getQueryParameters().getFirst("start_date");
        String stop = info.getQueryParameters().getFirst("end_date");
        JsonObject report = ReportGenerator.getReport(id,start,stop);
        return Response.status(Response.Status.OK).entity(report.toString()).build();
    }





}
