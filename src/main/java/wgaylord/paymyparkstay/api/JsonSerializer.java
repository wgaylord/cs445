package wgaylord.paymyparkstay.api;

import wgaylord.paymyparkstay.interfaces.NotesBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.OrdersBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.VisitorBoundaryInterface;
import wgaylord.paymyparkstay.managers.NotesManager;
import wgaylord.paymyparkstay.managers.OrdersManager;
import wgaylord.paymyparkstay.managers.VisitorsManager;
import wgaylord.paymyparkstay.objects.Note;
import wgaylord.paymyparkstay.objects.Order;
import wgaylord.paymyparkstay.objects.Park;
import wgaylord.paymyparkstay.objects.Visitor;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;

public class JsonSerializer {

    private static final VisitorBoundaryInterface vbi = new VisitorsManager();
    private static final NotesBoundaryInterface nbi = new NotesManager();
    private static final OrdersBoundaryInterface obi = new OrdersManager();
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static JsonObject SerializeParkView(Park park){
        JsonObjectBuilder Jpark = Json.createObjectBuilder();
        Jpark.add("pid",park.getID().toString());

        JsonObjectBuilder locInfo = Json.createObjectBuilder();
        locInfo.add("name",park.getName());
        locInfo.add("region",park.getRegion());
        locInfo.add("address",park.getAddress());
        locInfo.add("phone",park.getPhone());
        locInfo.add("web",park.getWebSite());

        JsonObjectBuilder Geo = Json.createObjectBuilder();
        Geo.add("lat",park.getLatitude());
        Geo.add("lng",park.getLongitude());

        locInfo.add("geo",Geo.build());

        Jpark.add("location_info",locInfo.build());
        return Jpark.build();
    }
    public static JsonObject SerializeParkDetail(Park park){
        JsonObjectBuilder jPark = Json.createObjectBuilder();
        jPark.add("pid",park.getID().toString());

        JsonObjectBuilder locInfo = Json.createObjectBuilder();
        locInfo.add("name", park.getName());
        locInfo.add("region", park.getRegion());
        locInfo.add("address", park.getAddress());
        locInfo.add("phone", park.getPhone());
        locInfo.add("web", park.getWebSite());

        JsonObjectBuilder Geo = Json.createObjectBuilder();
        Geo.add("lat", park.getLatitude());
        Geo.add("lng", park.getLongitude());

        JsonObjectBuilder payment = Json.createObjectBuilder();
        payment.add("motorcycle",Json.createArrayBuilder().add(park.getFee("motorcycle")[0]).add(park.getFee("motorcycle")[1]));
        payment.add("car",Json.createArrayBuilder().add(park.getFee("car")[0]).add(park.getFee("car")[1]));
        payment.add("rv",Json.createArrayBuilder().add(park.getFee("rv")[0]).add(park.getFee("rv")[1]));

        jPark.add("payment_info",payment);

        locInfo.add("geo", Geo.build());

        jPark.add("location_info", locInfo.build());

        return jPark.build();
    }

    public static JsonObject SerializeParkNote(Note note){
        JsonObjectBuilder jnote = Json.createObjectBuilder();
        jnote.add("nid",note.getId().toString());
        jnote.add("pid",note.getPid().toString());
        jnote.add("vid",note.getVid().toString());
        jnote.add("date",DATE_FORMAT.format(note.getDate()));
        jnote.add("title",note.getTitle());
        jnote.add("text",note.getMessage());

        return jnote.build();
    }


    public static JsonObject SerializeOrder(Order order){
        JsonObjectBuilder Jorder = Json.createObjectBuilder();
        Jorder.add("oid",order.getId().toString());
        Jorder.add("pid",order.getPid().toString());
        Jorder.add("date",DATE_FORMAT.format(order.getVist_date()));
        Jorder.add("type",order.getType());
        Jorder.add("amount",order.getAmount());

        return Jorder.build();
    }

    public static JsonObject SerlizeOrderDetails(Order order){
        JsonObjectBuilder Jorder = Json.createObjectBuilder();
        Jorder.add("oid",order.getId().toString());
        Jorder.add("pid",order.getPid().toString());
        Jorder.add("date",DATE_FORMAT.format(order.getVist_date()));
        Jorder.add("amount",order.getAmount());
        Jorder.add("vid",order.getVid().toString());

        JsonObjectBuilder vehicle = Json.createObjectBuilder();
        vehicle.add("state",order.getState());
        vehicle.add("plate",order.getPlate());
        vehicle.add("type",order.getType());

        Jorder.add("vehicle",vehicle.build());
        Visitor vist = vbi.getVisitor(order.getVid());

        JsonObjectBuilder payment = Json.createObjectBuilder();
        payment.add("card","xxxxxxxxxxx"+order.getCard().substring(11));
        payment.add("name_on_card",order.getName_on_card());
        payment.add("expiration_date",order.getExpiration_date());
        payment.add("zip",order.getZip());

        JsonObjectBuilder visitor = Json.createObjectBuilder();
        visitor.add("name",vist.getName());
        visitor.add("email",vist.getEmail());
        visitor.add("payment_info",payment.build());

        Jorder.add("visitor",visitor.build());

        JsonObjectBuilder processing = Json.createObjectBuilder();
        processing.add("card_transaction_id",order.getCard_transaction_id());
        processing.add("date_and_time",DATE_FORMAT.format(order.getDate_and_time()));

        Jorder.add("payment_processing",processing);

        return Jorder.build();
    }

    public static JsonObject SerializeVisitor(Visitor visitor){
        JsonObjectBuilder Jvist = Json.createObjectBuilder();
        Jvist.add("vid",visitor.getId().toString());
        Jvist.add("name",visitor.getName());
        Jvist.add("email",visitor.getEmail());
        return Jvist.build();
    }

    public static JsonObject SerlizeVisitorDetails(Visitor visitor){
        JsonObjectBuilder Jvist = Json.createObjectBuilder();
        JsonArrayBuilder Jorders = Json.createArrayBuilder();
        JsonArrayBuilder Jnotes = Json.createArrayBuilder();
        Jvist.add("vid",visitor.getId().toString());
        Order[] orders = obi.getOrdersFromVistor(visitor.getId());
        for (Order order : orders) {
            Jorders.add(SerializeOrderForVisitors(order));
        }

        Note[] notes = nbi.getNotesFromVistor(visitor.getId());
        System.out.println(notes.length+" "+NotesManager.notes.size());
        for (Note note : notes) {
            Jnotes.add(SerializeNoteForVisitors(note));
        }
        Jvist.add("orders",Jorders);
        Jvist.add("notes",Jnotes);
        Jvist.add("visitor",Json.createObjectBuilder().add("name",visitor.getName()).add("email",visitor.getEmail()));

        return Jvist.build();
    }


    private static JsonObject SerializeOrderForVisitors(Order order){
        JsonObjectBuilder Jorder = Json.createObjectBuilder();
        Jorder.add("oid",order.getId().toString());
        Jorder.add("pid",order.getPid().toString());
        Jorder.add("date",DATE_FORMAT.format(order.getVist_date()));
        return Jorder.build();
    }

    private static JsonObject SerializeNoteForVisitors(Note note){
        JsonObjectBuilder Jnote = Json.createObjectBuilder();
        Jnote.add("nid",note.getId().toString());
        Jnote.add("pid",note.getPid().toString());
        Jnote.add("date",DATE_FORMAT.format(note.getDate()));
        Jnote.add("title",note.getTitle());
        return Jnote.build();
    }

    public static JsonObject SeralizeNoteDetails(Note note){
        JsonObjectBuilder jnote = Json.createObjectBuilder();
        jnote.add("nid",note.getId().toString());
        jnote.add("pid",note.getPid().toString());
        jnote.add("vid",note.getVid().toString());
        jnote.add("date",DATE_FORMAT.format(note.getDate()));
        jnote.add("title",note.getTitle());
        jnote.add("text",note.getMessage());
        return jnote.build();
    }

    public static JsonObject SerializeNoteForParks(Note note){
        JsonObjectBuilder Jnote = Json.createObjectBuilder();
        Jnote.add("nid",note.getId().toString());
        Jnote.add("date",DATE_FORMAT.format(note.getDate()));
        Jnote.add("title",note.getTitle());
        return Jnote.build();
    }


    public static Response buildDataValidationError(String detail, String instance){
        JsonObjectBuilder error = Json.createObjectBuilder();
        error.add("type","http://cs.iit.edu/~virgil/cs445/project/api/problems/data-validation");
        error.add("title","Your request data didn't pass validation");
        error.add("detail",detail);
        error.add("status",400);
        error.add("instance",instance);
        return Response.status(Response.Status.BAD_REQUEST).entity(error.build().toString()).build();
    }
}
