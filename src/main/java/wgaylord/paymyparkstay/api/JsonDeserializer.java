package wgaylord.paymyparkstay.api;

import wgaylord.paymyparkstay.objects.Park;

import javax.json.JsonObject;

public class JsonDeserializer {
    public static final String[] acceptableVehicles = new String[] {"motorcycle","car","rv"};

    public static Park DeserializerPark(JsonObject jobj, Park newPark){

        JsonObject loc_info = jobj.getJsonObject("location_info");
        JsonObject payment_info = jobj.getJsonObject("payment_info");
        if(loc_info == null) {
            throw new RuntimeException("No location_info");
        }
        String name = loc_info.getString("name");
        String address = loc_info.getString("address");
        String phone = loc_info.getString("phone");
        String web = loc_info.getString("web");
        String region;
        JsonObject geo = loc_info.getJsonObject("geo");
        if(geo == null){
            throw new RuntimeException("Geo information is required but missing in your request");
        }
        try {
            double lat = geo.getJsonNumber("lat").doubleValue();
            double lng = geo.getJsonNumber("lng").doubleValue();
            newPark.setLatitude(lat);
            newPark.setLongitude(lng);
        }catch(NullPointerException exception){
            throw new RuntimeException("Geo information is required but missing in your request");
        }
        try{
            region = loc_info.getString("region");
        }catch (NullPointerException exception){
            region = "";
        }
        if(name.length() > 0){
            newPark.setName(name);
        }else{
            throw new RuntimeException("All parks must have a name");
        }
        if(web.length() > 0){
            newPark.setWebSite(web);
        }else{
            throw new RuntimeException("All parks must have a website");
        }
        if(address.length() > 0){
            newPark.setAddress(address);
        }else{
            throw new RuntimeException("All parks must have an address");
        }
        for (String acceptableVehicle : acceptableVehicles) {
            double[] temporayAmounts = new double[2];
            temporayAmounts[0] = payment_info.getJsonArray(acceptableVehicle).getJsonNumber(0).doubleValue();
            temporayAmounts[1] = payment_info.getJsonArray(acceptableVehicle).getJsonNumber(1).doubleValue();
            if (temporayAmounts[0] >= 0 & temporayAmounts[1] >= 0) {
                newPark.updateFee(acceptableVehicle, temporayAmounts);
            } else {
                throw new RuntimeException("All payment data must be a number greater than or equal to zero");
            }
        }

        newPark.setPhone(phone);
        newPark.setRegion(region);

        return newPark;
    }

}
