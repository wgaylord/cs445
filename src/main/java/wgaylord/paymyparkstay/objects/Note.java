package wgaylord.paymyparkstay.objects;

import java.util.Date;
import java.util.UUID;

public class Note {

    private final UUID id;
    private UUID pid;
    private UUID vid;
    private Date date;
    private String title = "";
    private String message = "";
    private boolean deleted;

    public Note() {
        this.id = UUID.randomUUID();
    }

    public void setPid(UUID pid) {
        this.pid = pid;
    }

    public void setVid(UUID vid) {
        this.vid = vid;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UUID getId() {
        return id;
    }

    public UUID getPid() {
        return pid;
    }

    public UUID getVid() {
        return vid;
    }

    public Date getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDeleted(){
        return this.deleted;
    }

    public void setDeleted(){
        this.deleted = true;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
