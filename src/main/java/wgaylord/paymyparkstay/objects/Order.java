package wgaylord.paymyparkstay.objects;

import java.util.Date;
import java.util.UUID;

public class Order {

    //IDs for relating to other objects
    private final UUID id;
    private UUID pid;
    private UUID vid;

    //Vehicle info
    private String State = "";
    private String Plate = "";
    private String type = "";

    private double amount = 0;

    private Date vist_date;

    //Payment Info
    private int zip = 0;
    private String expiration_date;
    private String name_on_card = "";
    private String card = "";

    //Payment_proccesing
    private String card_transaction_id = "";
    private Date date_and_time;


    public Order() {
        this.id = UUID.randomUUID();
    }

    public UUID getPid() {
        return this.pid;
    }

    public void setPid(UUID pid) {
        this.pid = pid;
    }

    public UUID getVid() {
        return vid;
    }

    public void setVid(UUID vid) {
        this.vid = vid;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPlate() {
        return Plate;
    }

    public void setPlate(String plate) {
        Plate = plate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getName_on_card() {
        return name_on_card;
    }

    public void setName_on_card(String name_on_card) {
        this.name_on_card = name_on_card;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCard_transaction_id() {
        return card_transaction_id;
    }

    public void setCard_transaction_id(String card_transaction_id) {
        this.card_transaction_id = card_transaction_id;
    }

    public Date getDate_and_time() {
        return date_and_time;
    }

    public void setDate_and_time(Date date_and_time) {
        this.date_and_time = date_and_time;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getVist_date() {
        return vist_date;
    }

    public void setVist_date(Date vist_date) {
        this.vist_date = vist_date;
    }
}
