package wgaylord.paymyparkstay.objects;

import java.util.UUID;

public class Visitor {

    private final UUID id;

    private String name = "";
    private final String email;


    public Visitor(String email) {
        this.id = UUID.nameUUIDFromBytes(email.getBytes());
        this.email = email;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }


}
