package wgaylord.paymyparkstay.objects;

import java.util.HashMap;
import java.util.UUID;

public class Park {

    private String Name = "";
    private String Region = "";
    private String Address = "";
    private String Phone = "";
    private String WebSite = "";
    private double Latitude;
    private double Longitude;
    private final UUID ID;
    private boolean active;
    private final HashMap<String,double[]> feeMap;

    public Park(){
        ID = UUID.randomUUID();
        feeMap = new HashMap<>();
        active = true;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setWebSite(String webSite) {
        WebSite = webSite;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public void updateFee(String type,double[] rates) {
        this.feeMap.put(type,rates);
    }

    public void setName(String name){
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public String getRegion() {
        return Region;
    }

    public String getAddress() {
        return Address;
    }

    public String getPhone() {
        return Phone;
    }

    public String getWebSite() {
        return WebSite;
    }

    public double getLatitude() {
        return Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public UUID getID() {
        return ID;
    }


    public double[] getFee(String type) {
        return feeMap.get(type);
    }


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
