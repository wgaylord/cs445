package wgaylord.paymyparkstay.managers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wgaylord.paymyparkstay.objects.Note;

import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class NotesManagerTest {

    private NotesManager manager;

    @BeforeEach
    void setup(){
        manager = new NotesManager();
        NotesManager.notes = new Vector<>();
    }

    @Test
    void GetAllNotes_WhenNoNotes(){
        Note[] test = new Note[0];
        assertArrayEquals(test,manager.getNotes());
    }

    @Test
    void GetAllNotes_AfterNoteAdded(){
        Note note = new Note();
        note.setTitle("Test Title");
        note.setVid(UUID.randomUUID());
        manager.addNote(note);
        Note[] testArray = new Note[1];
        testArray[0] = note;
        assertArrayEquals(testArray,manager.getNotes());
    }

    @Test
    void GetAllNotes_AfterNoteAddedTwice(){
        Note note = new Note();
        note.setTitle("Test Title");
        note.setVid(UUID.randomUUID());
        manager.addNote(note);
        manager.addNote(note);
        Note[] testArray = new Note[1];
        testArray[0] = note;
        assertArrayEquals(testArray,manager.getNotes());
    }


    @Test
    void GetNoteByUUID(){
        Note Note = new Note();

        manager.addNote(Note);
        assertEquals(Note,manager.getNote( Note.getId()));
    }
    @Test
    void SearchNotes(){
        Note note = new Note();
        note.setTitle("Testing");
        note.setDate(new Date());
        note.setMessage("Test Message");
        manager.addNote(note);
        Note Note1 = new Note();
        Note1.setTitle("Note!");
        manager.addNote(Note1);

        Note[] testArray = new Note[0];


        assertArrayEquals(testArray,manager.searchNotes("Doe"));
        testArray = new Note[1];
        testArray[0] = note;

        assertArrayEquals(testArray,manager.searchNotes("Test"));

    }

    @Test
    void GetNoteByParkUUID(){
        Note note = new Note();
        UUID pid = UUID.randomUUID();
        note.setPid(pid);
        manager.addNote(note);
        assertEquals(note,manager.getNotesFromPark(pid)[0]);
    }


    @Test
    void GetNoteByVisitorUUID(){
        Note note = new Note();
        UUID vid = UUID.randomUUID();
        note.setVid(vid);
        manager.addNote(note);
        assertEquals(note,manager.getNotesFromVistor(vid)[0]);
    }

    
}