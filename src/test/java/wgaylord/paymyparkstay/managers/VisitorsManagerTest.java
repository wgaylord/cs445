package wgaylord.paymyparkstay.managers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wgaylord.paymyparkstay.objects.Visitor;

import java.util.UUID;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class VisitorsManagerTest {

    private VisitorsManager manager;

    @BeforeEach
    void setup(){
        manager = new VisitorsManager();
        VisitorsManager.visitors = new Vector<>();
    }

    @Test
    void GetAllVisitors_WhenNoVisitors(){
        Visitor[] test = new Visitor[0];
        assertArrayEquals(test,manager.getVisitors());
    }

    @Test
    void GetAllVisitors_AfterVisitorAdded(){
        Visitor vist = new Visitor("test@example.com");
        manager.addVisitor(vist);
        Visitor[] testArray = new Visitor[1];
        testArray[0] = vist;
        assertArrayEquals(testArray,manager.getVisitors());
    }

    @Test
    void GetAllVisitors_AfterVisitorAddedTwice(){
        Visitor vist = new Visitor("test@example.com");
        manager.addVisitor(vist);
        manager.addVisitor(vist);
        vist.setName("Joe");
        manager.addVisitor(vist);
        Visitor[] testArray = new Visitor[1];
        testArray[0] = vist;
        assertArrayEquals(testArray,manager.getVisitors());
    }

    @Test
    void GetVisitorByUUID(){
        Visitor vist = new Visitor("test@example.com");
        manager.addVisitor(vist);
        assertEquals(vist,manager.getVisitor(UUID.nameUUIDFromBytes("test@example.com".getBytes())));
    }

    @Test
    void SearchVisitors(){
        Visitor vist = new Visitor("test@example.com");
        vist.setName("Jane Doe");
        manager.addVisitor(vist);
        Visitor vist1 = new Visitor("joe@example.com");
        vist1.setName("Joe Doe");
        manager.addVisitor(vist1);
        Visitor vist2 = new Visitor("todd@example.com");
        vist2.setName("Todd Doe");
        manager.addVisitor(vist2);
        Visitor[] testArray = new Visitor[3];
        testArray[0] = vist;
        testArray[1] = vist1;
        testArray[2] = vist2;

        assertArrayEquals(testArray,manager.searchVisitors("Doe"));
        testArray = new Visitor[1];
        testArray[0] = vist;

        assertArrayEquals(testArray,manager.searchVisitors("Jane"));

    }

}