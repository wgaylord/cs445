package wgaylord.paymyparkstay.managers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wgaylord.paymyparkstay.objects.Order;

import java.util.UUID;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class OrdersManagerTest {

    private OrdersManager manager;

    @BeforeEach
    void setup(){
        manager = new OrdersManager();
        OrdersManager.orders = new Vector<>();
    }

    @Test
    void GetAllOrders_WhenNoOrders(){
        Order[] test = new Order[0];
        assertArrayEquals(test,manager.getOrders());
    }

    @Test
    void GetAllOrders_AfterOrderAdded(){
        Order order = new Order();
        manager.addOrder(order);
        Order[] testArray = new Order[1];
        testArray[0] = order;
        assertArrayEquals(testArray,manager.getOrders());
    }

    @Test
    void GetOrderByUUID(){
        Order order = new Order();

        manager.addOrder(order);
        assertEquals(order,manager.getOrder( order.getId()));
    }

    @Test
    void ReturnNullIfNoOrderFound() {
        Order order = new Order();

        manager.addOrder(order);
        assertNull(manager.getOrder(UUID.randomUUID()));
    }
    @Test
    void SearchOrders(){
        Order order = new Order();
        order.setPlate("60MPG");
        manager.addOrder(order);
        Order order1 = new Order();
        order1.setPlate("Test");
        manager.addOrder(order1);

        Order[] testArray = new Order[0];


        assertArrayEquals(testArray,manager.searchOrders("Doe"));
        testArray = new Order[1];
        testArray[0] = order;

        assertArrayEquals(testArray,manager.searchOrders("60MPG"));

    }

    @Test
    void GetOrderByVisitorUUID(){
        Order order = new Order();
        UUID vID = UUID.randomUUID();
        order.setVid(vID);
        manager.addOrder(order);
        assertEquals(order,manager.getOrdersFromVistor(vID)[0]);
    }

    @Test
    void GetNOOrderByVisitorUUID(){
        Order order = new Order();
        UUID vID = UUID.randomUUID();
        order.setVid(vID);
        manager.addOrder(order);
        assertArrayEquals(new Order[0],manager.getOrdersFromVistor(UUID.randomUUID()));
    }

    @Test
    void GetNOOrderByVisitorUUIDAndParkUUID(){
        Order order = new Order();
        UUID vID = UUID.randomUUID();
        UUID pID = UUID.randomUUID();
        order.setVid(vID);
        order.setPid(pID);
        manager.addOrder(order);
        assertArrayEquals(new Order[0],manager.getOrdersFromVistorAndPark(UUID.randomUUID(),pID));
        order = new Order();
        vID = UUID.randomUUID();
        pID = UUID.randomUUID();
        order.setVid(vID);
        order.setPid(pID);
        manager.addOrder(order);
        Order[] orders = new Order[1];
        orders[0] = order;
        assertArrayEquals(orders,manager.getOrdersFromVistorAndPark(vID,pID));
    }

    @Test
    void GetOrderByParkUUID(){
        Order order = new Order();
        UUID pID = UUID.randomUUID();
        order.setPid(pID);
        manager.addOrder(order);
        assertArrayEquals(new Order[0],manager.getOrdersFromPark(UUID.randomUUID()));
        assertArrayEquals(new Order[]{order},manager.getOrdersFromPark(pID));

    }

}