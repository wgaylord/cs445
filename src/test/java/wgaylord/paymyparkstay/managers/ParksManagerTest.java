package wgaylord.paymyparkstay.managers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wgaylord.paymyparkstay.objects.Park;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ParksManagerTest {

    private final ParksManager manager = new ParksManager();

    @BeforeEach
    void setup(){
        Park[] parks = manager.getParks();
        for (Park park : parks) {
            manager.deletePark(park.getID());
        }
    }

    @Test
    void GetAllParks_WhenNoParks(){
        Park[] test = new Park[0];
        assertArrayEquals(test,manager.getParks());
    }

    @Test
    void GetAllParks_AfterParkAdded(){
        Park park = new Park();
        manager.addPark(park);
        Park[] testArray = new Park[1];
        testArray[0] = park;
        assertArrayEquals(testArray,manager.getParks());
    }

    @Test
    void GetAllParks_AfterParkAddedTwice(){
        Park park = new Park();
        manager.addPark(park);
        manager.addPark(park);
        Park[] testArray = new Park[1];
        testArray[0] = park;
        assertArrayEquals(testArray,manager.getParks());
    }

    @Test
    void GetParkByUUID(){
        Park park = new Park();
        UUID parkID = park.getID();
        manager.addPark(park);
        assertEquals(park,manager.getPark(parkID));
    }

    @Test
    void SearchParks(){
        Park park = new Park();
        park.setName("Test1");
        manager.addPark(park);
        Park park1 = new Park();
        park1.setName("Test2");
        manager.addPark(park1);
        Park park2 = new Park();
        park2.setName("Test2");
        manager.addPark(park2);
        Park[] testArray = new Park[3];
        testArray[0] = park;
        testArray[1] = park1;
        testArray[2] = park2;


        assertArrayEquals(testArray,manager.searchParks("Test"));
        testArray = new Park[1];
        testArray[0] = park;

        assertArrayEquals(testArray,manager.searchParks("Test1"));


    }

    @Test
    void DeletePark(){
        Park park = new Park();
        manager.addPark(park);
        Park[] testArray = new Park[0];
        manager.deletePark(park.getID());
        assertArrayEquals(testArray,manager.getParks());
    }
}