package wgaylord.paymyparkstay.api;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import wgaylord.paymyparkstay.interfaces.NotesBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.OrdersBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.ParksBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.VisitorBoundaryInterface;
import wgaylord.paymyparkstay.managers.NotesManager;
import wgaylord.paymyparkstay.managers.OrdersManager;
import wgaylord.paymyparkstay.managers.ParksManager;
import wgaylord.paymyparkstay.managers.VisitorsManager;
import wgaylord.paymyparkstay.objects.Note;
import wgaylord.paymyparkstay.objects.Order;
import wgaylord.paymyparkstay.objects.Park;
import wgaylord.paymyparkstay.objects.Visitor;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JsonSerializerTest {

    Park park;
    Note note;
    Visitor visitor;
    Order order;

    @BeforeAll
    void setUp() {
        ParksBoundaryInterface pbi = new ParksManager();
        NotesBoundaryInterface nbi = new NotesManager();
        VisitorBoundaryInterface vbi = new VisitorsManager();
        OrdersBoundaryInterface obi = new OrdersManager();




        park = new Park();
        park.setName("Test");
        park.setRegion("Test Region");
        park.setAddress("Test Address");
        park.setPhone("123 456 7890");
        park.setLatitude(10.00);
        park.setLongitude(20.00);
        double[] prices = new double[2];
        prices[0] = 10.0;
        prices[1] = 10.0;
        park.updateFee("motorcycle",prices);
        park.updateFee("car",prices);
        park.updateFee("rv",prices);
        pbi.addPark(park);

        note = new Note();
        note.setPid(park.getID());
        note.setMessage("Message");
        note.setTitle("Title");

        visitor = new Visitor("test@example.com");
        visitor.setName("Jane Doe");

        note.setVid(visitor.getId());
        note.setDate(new Date());

        nbi.addNote(note);
        vbi.addVisitor(visitor);

        order = new Order();
        order.setPid(park.getID());
        order.setAmount(4.50);
        order.setDate_and_time(new Date());
        order.setZip(60616);
        order.setCard_transaction_id("123-4567-89");
        order.setCard("373456789045678");
        order.setName_on_card("Jane Doe");
        order.setExpiration_date("01/01");
        order.setVid(visitor.getId());
        order.setVist_date(new Date());
        order.setType("rv");
        order.setState("IL");
        order.setPlate("GOCUBS");
        obi.addOrder(order);
    }

    @Test
    void serializeParkView() {
        String expected = "{\n" +
                "    \"pid\": \""+park.getID()+"\",\n" +
                "    \"location_info\": {\n" +
                "      \"name\": \"Test\",\n" +
                "      \"region\": \"Test Region\",\n" +
                "      \"address\": \"Test Address\",\n" +
                "      \"phone\": \"123 456 7890\",\n" +
                "      \"web\": \"\",\n" +
                "      \"geo\": {\n" +
                "        \"lat\": 10.0,\n" +
                "        \"lng\": 20.0\n" +
                "      }\n" +
                "    }\n" +
                "  }";

        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jPark = JsonSerializer.SerializeParkView(park);

        assertEquals(jobj,jPark);
    }

    @Test
    void serializeParkDetail() {

        String expected = "{\n" +
                "    \"pid\": \""+park.getID()+"\",\n" +
                "    \"location_info\": {\n" +
                "      \"name\": \"Test\",\n" +
                "      \"region\": \"Test Region\",\n" +
                "      \"address\": \"Test Address\",\n" +
                "      \"phone\": \"123 456 7890\",\n" +
                "      \"web\": \"\",\n" +
                "      \"geo\": {\n" +
                "        \"lat\": 10.0,\n" +
                "        \"lng\": 20.0\n" +
                "      }\n" +
                "    }\n," + "\"payment_info\":{\"motorcycle\":[10.0,10.0],\"car\":[10.0,10.0],\"rv\":[10.0,10.0]}}"+
                "  }";

        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jPark = JsonSerializer.SerializeParkDetail(park);

        assertEquals(jobj,jPark);
    }

    @Test
    void serializeParkNote() {
        String expected = "{\n" +
                "      \"nid\": \""+note.getId()+"\",\n" +
                "      \"pid\": \""+note.getPid()+"\",\n" +
                "      \"vid\": \""+note.getVid()+"\",\n" +
                "      \"date\": \""+JsonSerializer.DATE_FORMAT.format(note.getDate())+"\",\n" +
                "      \"title\": \"Title\",\n" +
                "      \"text\": \"Message\"\n" +
                "    }";
        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jNote = JsonSerializer.SerializeParkNote(note);

        assertEquals(jobj,jNote);
    }

    @Test
    void serializeOrder() {
        String expected = "\n" +
                "      {\n" +
                "        \"oid\": \""+order.getId()+"\",\n" +
                "        \"pid\": \""+order.getPid()+"\",\n" +
                "        \"date\": \""+JsonSerializer.DATE_FORMAT.format(order.getVist_date())+"\",\n" +
                "        \"type\": \"rv\",\n" +
                "        \"amount\": 4.5\n" +
                "      }";

        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jorder = JsonSerializer.SerializeOrder(order);

        assertEquals(jobj,jorder);
    }

    @Test
    void serlizeOrderDetails() {
        String expected = "{\n" +
                "   \"oid\": \""+order.getId()+"\",\n" +
                "    \"pid\": \""+order.getPid()+"\",\n" +
                "    \"amount\": 4.5,\n" +
                "    \"vid\": \""+note.getVid()+"\",\n" +
                "     \"date\": \""+JsonSerializer.DATE_FORMAT.format(order.getVist_date())+"\",\n" +
                "    \"vehicle\": {\n" +
                "    \"state\": \"IL\",\n" +
                "    \"plate\": \"GOCUBS\",\n" +
                "    \"type\": \"rv\"\n" +
                "    },\n" +
                "    \"visitor\": {\n" +
                "    \"name\": \"Jane Doe\",\n" +
                "    \"email\":\"test@example.com\","+
                "  \"payment_info\":{"+
                "    \"card\": \"xxxxxxxxxxx5678\",\n" +
                "    \"name_on_card\": \"Jane Doe\",\n" +
                "    \"expiration_date\": \"01/01\",\n" +
                "    \"zip\": 60616\n" +
                "    }\n" +
                "    },\n" +
                "    \"payment_processing\": {\n" +
                "    \"card_transaction_id\": \"123-4567-89\",\n" +
                "    \"date_and_time\": \""+JsonSerializer.DATE_FORMAT.format(order.getVist_date())+"\"\n" +
                "    }\n" +
                "    }";

        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jorder = JsonSerializer.SerlizeOrderDetails(order);

        assertEquals(jobj,jorder);

    }

    @Test
    void serializeVisitor() {
        String expected = "{\n" +
                "        \"vid\":\""+visitor.getId()+"\",\n" +
                "        \"name\": \"Jane Doe\",\n" +
                "        \"email\": \"test@example.com\"\n" +
                "      }";
        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jvistor = JsonSerializer.SerializeVisitor(visitor);

        assertEquals(jobj,jvistor);

    }

    @Test
    void serlizeVisitorDetails() {
        String expected = "{\n" +
                "        \"vid\":\""+visitor.getId()+"\",\n" +
                "    \"visitor\": {\n" +
                "        \"name\": \"Jane Doe\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "    },\n" +
                "    \"orders\": [{\n" +
                "    \"oid\": \""+order.getId()+"\",\n" +
                "    \"pid\": \""+order.getPid()+"\",\n" +
                "    \"date\": \""+JsonSerializer.DATE_FORMAT.format(order.getVist_date())+"\"\n" +
                "    }"+
                "    ],\n" +
                "    \"notes\": [{\n" +
                "    \"nid\": \""+note.getId()+"\",\n" +
                "    \"pid\": \""+note.getPid()+"\",\n" +
                "    \"date\": \""+JsonSerializer.DATE_FORMAT.format(note.getDate())+"\",\n" +
                "    \"title\": \"Title\"\n" +
                "    }]\n" +
                "    }";
        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jvistor = JsonSerializer.SerlizeVisitorDetails(visitor);

        assertEquals(jobj,jvistor);
    }


    @Test
    void seralizeNoteDetails() {
        String expected = "{\n" +
                "      \"nid\": \""+note.getId()+"\",\n" +
                "      \"pid\": \""+note.getPid()+"\",\n" +
                "      \"vid\": \""+note.getVid()+"\",\n" +
                "    \"date\": \""+JsonSerializer.DATE_FORMAT.format(note.getDate())+"\",\n" +
                "      \"title\": \"Title\",\n" +
                "      \"text\": \"Message\"\n" +
                "    }";

        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jnote = JsonSerializer.SeralizeNoteDetails(note);

        assertEquals(jobj,jnote);
    }

    @Test
    void serializeNoteForParks() {
        String expected = "{\n \"nid\": \""+note.getId()+"\",\n" +
                " \"date\": \""+JsonSerializer.DATE_FORMAT.format(note.getDate())+"\",\n" +
                "  \"title\": \"Title\"\n}";

        JsonReader jsonReader = Json.createReader(new StringReader(expected));
        JsonObject jobj = jsonReader.readObject();

        JsonObject jnote = JsonSerializer.SerializeNoteForParks(note);

        assertEquals(jobj,jnote);
    }


}