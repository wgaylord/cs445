package wgaylord.paymyparkstay.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wgaylord.paymyparkstay.interfaces.OrdersBoundaryInterface;
import wgaylord.paymyparkstay.interfaces.ParksBoundaryInterface;
import wgaylord.paymyparkstay.managers.OrdersManager;
import wgaylord.paymyparkstay.managers.ParksManager;
import wgaylord.paymyparkstay.objects.Order;
import wgaylord.paymyparkstay.objects.Park;

import javax.json.*;
import java.io.StringReader;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class ReportGeneratorTest {


    @BeforeEach
    void setup(){
       ParksManager.parks = new Vector<>();
    }

    @Test
    void getReports() {
        String test = "[{\"rid\": 907,\"name\": \"Admissions report\"}, {\"rid\": 911,\"name\": \"Revenue report\"}]";
        JsonReader jsonReader = Json.createReader(new StringReader(test));
        JsonArray jobj = jsonReader.readArray();
        assertEquals(jobj,ReportGenerator.getReports());
    }

    @Test
    void getAdmissionReport() {
        ParksBoundaryInterface pbi = new ParksManager();
        Park park = new Park();
        park.setName("Test Park");
        pbi.addPark(park);
        String AddmisionReport =  "{\"rid\": 907,\"name\": \"Admissions report\",\"start_date\": \"\",\"end_date\": \"\",\"total_admissions\": 0,\"detail_by_park\": [{\"pid\":\""+park.getID()+"\",\"name\":\"Test Park\",\"admissions\":0}]}";
        JsonReader jsonReader = Json.createReader(new StringReader(AddmisionReport));
        JsonObject jobj = jsonReader.readObject();

        assertEquals(jobj,ReportGenerator.getReport(907,"",""));

    }

    @Test
    void getRevenueReport() {
        ParksBoundaryInterface pbi = new ParksManager();
        OrdersBoundaryInterface obi = new OrdersManager();

        Park park = new Park();
        park.setName("Test Park");
        pbi.addPark(park);
        Order order = new Order();
        order.setAmount(14.0);
        order.setPid(park.getID());
        obi.addOrder(order);
        String AddmisionReport =  "{\"rid\": 911,\"name\": \"Revenue report\",\"start_date\": \"\",\"end_date\": \"\",\"total_orders\": 1,\"total_revenue\":14.0,\"detail_by_park\": [{\"pid\":\""+park.getID()+"\",\"name\":\"Test Park\",\"revenue\":14.0,\"orders\":1}]}";
        JsonReader jsonReader = Json.createReader(new StringReader(AddmisionReport));
        JsonObject jobj = jsonReader.readObject();

        assertEquals(jobj,ReportGenerator.getReport(911,"",""));

    }
}